package cn.j3.myspring.controller;

import cn.j3.myspring.service.MyTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

import java.util.Map;

/**
 * @author J3-白起
 * @package cn.j3.myspring.controller
 * @createTime 2022/4/2 - 16:02
 * @description
 */
@RestController
@RequestMapping("/test")
public class MyTestController {

    public MyTestController() {
        System.out.println("=======MyTestController创建了========");
    }


    @Autowired
    private MyTestService service;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private WebApplicationContext webApplicationContext;


    @GetMapping("/")
    public String test() {
        System.out.println("=======service：{}" + service);
        System.out.println("=======applicationContext：{}" + applicationContext);
        System.out.println("=======webApplicationContext：{}" + webApplicationContext);

        Map<String, MyTestService> beansOfType = applicationContext.getBeansOfType(MyTestService.class);
        System.out.println(beansOfType);

        MyTestService myTestService1 = applicationContext.getBean(MyTestService.class);
        System.out.println(myTestService1);

        MyTestService myTestService2 = applicationContext.getParentBeanFactory().getBean(MyTestService.class);
        System.out.println(myTestService2);

        service.testMapper();
        return "success";
    }

}
