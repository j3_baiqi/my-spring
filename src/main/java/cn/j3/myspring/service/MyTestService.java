package cn.j3.myspring.service;

import cn.j3.myspring.mapper.MyTestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author J3-白起
 * @package cn.j3.myspring.service
 * @createTime 2022/4/2 - 16:04
 * @description
 */
@Service
public class MyTestService {

    public MyTestService(){
        System.out.println("=======MyTestService创建了========");
    }

    @Autowired
    private MyTestMapper mapper;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private WebApplicationContext webApplicationContext;

    public void testMapper(){
        System.out.println(mapper);
        System.out.println(applicationContext);
        System.out.println(webApplicationContext);
    }

}
